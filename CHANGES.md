# Cake Manager Submission

Submitted by David Russell<david.russell.scotland@gmail.com>

## How to run
* Execute `mvn spring-boot:run` from the `cake-manager` folder in a terminal
* Access `http://localhost:8282/cakes` in a web browser (or send data to the JSON endpoints)

## Changes
* Replaced Servlet with Spring Boot (personal preference / familiarity)
* Reimplemented application using Spring
    * controller layer providing the specified JSON and web endpoints
    * service layer to abstract interaction with data layer
    * Spring Data repository for interaction with the in-memory database
    * Freemarker template for web interface
* Cake entity is retained (though modified)
    * Changed table name from 'Employee' to 'Cake'
    * Remove unique constraints on all fields
        * Unique constraint on the id field has no effect, because a field annotated with `@Id` is designated the
        primary key (so inherently unique)
        * User might want to load multiple entries for a particular type of cake (e.g. regional variations on a recipe)
    * Changed 'employeeId' field to 'id'
    * Changed 'title' field to 'name' and 'image' field to 'imageUrl'
    * Used Lombok for getter/setter/constructor/etc. boilerplate
* Web interface uses JSON endpoints (invoked using jQuery) for input, rather than separate
`application/x-www-form-urlencoded` ones
* The sample data JSON specified in the original code sample is still used, but is loaded into the application
differently (see `com.waracle.cakemgr.referencedata` package for details)

## Extra features
* Editing and deletion of existing cake entries
    * Implemented as JSON HTTP `PUT`/`DELETE` endpoints, but the web interface has buttons to invoke the functionality
    (jQuery sends the appropriate request)
    
## Example JSON input

### POST /cakes (create new)
```
{
  "name": "Battenberg Cake",
  "description": "Sponge cake with distinctive checker board pattern, covered in marzipan",
  "imageUrl": "http://www.bbcgoodfood.com/sites/default/files/styles/recipe/public/recipe_images/recipe-image-legacy-id--545469_11.jpg"
}
```

### PUT /cakes (update existing)
```
{
  "id": 1,
  "name": "Birthday cake",
  "description": "an annual treat",
  "imageUrl": "http://cornandco.com/wp-content/uploads/2014/05/birthday-cake-popcorn.jpg"
}
```

### DELETE /cakes (remove existing)
```
{
  "id": 1
}
```

## Known issues
* Due to a classpath issue with the use of the /webapp folder, JAR files built from the repository do not work (they
deploy, but accessing the web interface causes an error). Running using Maven (as specified in the task spec) works fine.
* The application doesn't verify that the image URL actually points to an image (either at time of submission or time
of rendering), only that it starts with http and ends with `.png` or `.jpg`
* jQuery rendering of form validation errors from server relies on ES6 syntax, so won't work on old browsers
    * There's client-side validation on the forms so with the current functionality it's unlikely the server
    _will_ return any validation errors
* No CSRF (etc.) protection on the JSON endpoints