package com.waracle.cakemgr.service;

import com.waracle.cakemgr.dto.ExistingCakeDto;
import com.waracle.cakemgr.model.CakeEntity;
import com.waracle.cakemgr.model.CakeRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;

import static com.waracle.cakemgr.controller.CakeControllerTest.EXISTING_CAKE_DTO;
import static com.waracle.cakemgr.controller.CakeControllerTest.TEST_CAKE_DESCRIPTION;
import static com.waracle.cakemgr.controller.CakeControllerTest.TEST_CAKE_IMAGE;
import static com.waracle.cakemgr.controller.CakeControllerTest.TEST_CAKE_TITLE;
import static com.waracle.cakemgr.controller.CakeControllerTest.TEST_ENTITY_ID;
import static com.waracle.cakemgr.controller.CakeControllerTest.mockInputDto;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Unit tests for service implementation
 */
public class CakeServiceImplTest {

  @Mock
  private CakeRepository cakeRepositoryMock;

  private CakeServiceImpl cakeServiceImpl;

  private ArgumentCaptor<CakeEntity> argumentCaptor;

  @Before
  public void setUp() {
    initMocks(this);

    argumentCaptor = ArgumentCaptor.forClass(CakeEntity.class);
    cakeServiceImpl = new CakeServiceImpl(cakeRepositoryMock);

    CakeEntity testEntity = CakeEntity.builder().id(TEST_ENTITY_ID)
        .name(TEST_CAKE_TITLE)
        .description(TEST_CAKE_DESCRIPTION)
        .imageUrl(TEST_CAKE_IMAGE)
        .build();
    doReturn(Collections.singletonList(testEntity)).when(cakeRepositoryMock).findAll();
    doReturn(testEntity).when(cakeRepositoryMock).findOne(TEST_ENTITY_ID);
    doReturn(testEntity).when(cakeRepositoryMock).save(argumentCaptor.capture());
  }

  @Test
  public void testFindAllCakes() {
    List<ExistingCakeDto> response = cakeServiceImpl.getAllCakes();

    assertThat(response.size(), is(1));

    ExistingCakeDto outputDto = response.get(0);
    assertThat(outputDto.getId(), is(TEST_ENTITY_ID));
    assertThat(outputDto.getName(), is(TEST_CAKE_TITLE));
    assertThat(outputDto.getImageUrl(), is(TEST_CAKE_IMAGE));

    verify(cakeRepositoryMock).findAll();
  }

  @Test
  public void testSaveNewCake() {
    cakeServiceImpl.saveNewCake(mockInputDto);
    CakeEntity repositoryArgument = argumentCaptor.getValue();

    assertExpectedRepoArgument(repositoryArgument);
  }

  @Test
  public void testUpdateCake() {
    cakeServiceImpl.updateCake(EXISTING_CAKE_DTO);
    CakeEntity repositoryArgument = argumentCaptor.getValue();
    assertExpectedRepoArgument(repositoryArgument);
  }

  private void assertExpectedRepoArgument(CakeEntity repositoryArgument) {
    assertThat(repositoryArgument.getName(), is(TEST_CAKE_TITLE));
    assertThat(repositoryArgument.getDescription(), is(TEST_CAKE_DESCRIPTION));
    assertThat(repositoryArgument.getImageUrl(), is(TEST_CAKE_IMAGE));
  }
}
