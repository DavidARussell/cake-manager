package com.waracle.cakemgr.controller;

import com.waracle.cakemgr.dto.CakeDeletionDto;
import com.waracle.cakemgr.dto.ExistingCakeDto;
import com.waracle.cakemgr.dto.NewCakeDto;
import com.waracle.cakemgr.service.CakeService;
import com.waracle.cakemgr.service.CakeServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Unit tests for controller class
 * <p>
 * This doesn't test the web endpoints, it just tests the controller as a POJO
 */
public class CakeControllerTest {

  public static final String TEST_CAKE_TITLE = "Victoria Sponge";
  public static final String TEST_CAKE_DESCRIPTION = "Two layers of sponge cake with jam and buttercream between them, dusted with icing sugar.";
  public static final String TEST_CAKE_IMAGE = "http://www.bbcgoodfood.com/sites/default/files/styles/recipe/public/recipe_images/recipe-imageUrl-legacy-id--1001468_10.jpg";
  public static final int TEST_ENTITY_ID = 42;
  public static final NewCakeDto mockInputDto = new NewCakeDto(TEST_CAKE_TITLE, TEST_CAKE_DESCRIPTION, TEST_CAKE_IMAGE);
  public static final ExistingCakeDto EXISTING_CAKE_DTO = ExistingCakeDto.builder().id(TEST_ENTITY_ID)
      .title(TEST_CAKE_TITLE)
      .description(TEST_CAKE_DESCRIPTION)
      .image(TEST_CAKE_IMAGE)
      .build();
  private static final String TEST_ERROR_MESSAGE = "I am a test validation error";
  private static final ExistingCakeDto failureOutputDto = ExistingCakeDto.builder().errors(Collections.singletonList(TEST_ERROR_MESSAGE)).build();

  private static final CakeDeletionDto deletionDto = new CakeDeletionDto(TEST_ENTITY_ID);

  @Mock
  private CakeService cakeServiceMock;
  private CakeController cakeController;

  @Before
  public void setUp() {
    initMocks(this);
    cakeController = new CakeController(cakeServiceMock);

    doReturn(Collections.singletonList(EXISTING_CAKE_DTO)).when(cakeServiceMock).getAllCakes();
    doReturn(EXISTING_CAKE_DTO).when(cakeServiceMock).saveNewCake(mockInputDto);
  }

  @Test
  public void testGetAllCakes() {
    final List<ExistingCakeDto> controllerResponse = cakeController.getAllCakesJson();
    assertThat(controllerResponse.size(), is(1));
    assertThat(controllerResponse.get(0), is(EXISTING_CAKE_DTO));
    verify(cakeServiceMock, times(1)).getAllCakes();
  }

  @Test
  public void testSaveNewCake() {
    final ExistingCakeDto controllerResponse = cakeController.saveNewCakeJson(mockInputDto).getBody();
    assertThat(controllerResponse, is(EXISTING_CAKE_DTO));
    verify(cakeServiceMock, times(1)).saveNewCake(mockInputDto);
  }

  @Test
  public void testSaveNewCake_validationFailure() {
    doReturn(failureOutputDto).when(cakeServiceMock).saveNewCake(mockInputDto);
    final ResponseEntity<ExistingCakeDto> controllerResponse = cakeController.saveNewCakeJson(mockInputDto);
    assertThat(controllerResponse.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    assertThat(controllerResponse.getBody().getErrors().size(), is(1));
    assertThat(controllerResponse.getBody().getErrors().get(0), is(TEST_ERROR_MESSAGE));
  }

  @Test
  public void testDeleteCake_successful() throws Exception {
    final ResponseEntity controllerResponse = cakeController.deleteCake(deletionDto);
    assertThat(controllerResponse.getStatusCode(), is(HttpStatus.OK));
    verify(cakeServiceMock, times(1)).deleteCake(TEST_ENTITY_ID);
  }

  @Test
  public void testDeleteCake_unsuccessful() throws Exception {
    doThrow(CakeServiceException.cakeDoesNotExist(TEST_ENTITY_ID)).when(cakeServiceMock).deleteCake(TEST_ENTITY_ID);
    final ResponseEntity controllerResponse = cakeController.deleteCake(deletionDto);
    assertThat(controllerResponse.getStatusCode(), is(HttpStatus.BAD_REQUEST));
  }

  @Test
  public void testUpdateCake_successful() throws Exception {
    doReturn(EXISTING_CAKE_DTO).when(cakeServiceMock).updateCake(EXISTING_CAKE_DTO);
    final ResponseEntity controllerResponse = cakeController.updateCake(EXISTING_CAKE_DTO);
    assertThat(controllerResponse.getStatusCode(), is(HttpStatus.OK));
    assertThat(controllerResponse.getBody(), is(EXISTING_CAKE_DTO));
  }

  @Test
  public void testUpdateCake_unsuccessful() throws Exception {
    doReturn(failureOutputDto).when(cakeServiceMock).updateCake(EXISTING_CAKE_DTO);
    final ResponseEntity controllerResponse = cakeController.updateCake(EXISTING_CAKE_DTO);
    assertThat(controllerResponse.getStatusCode(), is(HttpStatus.BAD_REQUEST));
  }
}
