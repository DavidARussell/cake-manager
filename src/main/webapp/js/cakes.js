$(document).ready(function () {

    $('.btn-danger').click(function(){
        var cakeId = $(this).attr("data-cakeid");
        $.ajax({
            url: '/cakes',
            type: 'DELETE',
            contentType: 'application/json',
            data:JSON.stringify({
                id: cakeId
            }),
            success: function(){
                location.reload();
            },
            error: function(){
                bootbox.alert("Couldn't delete the selected cake - it may already have been deleted");
            }
        });
    });

    $('.btn-primary, .btn-success').click(function(){
        var cakeId = parseInt($(this).attr("data-cakeid"));
        var cakeName = $('#cakename-'+cakeId).text();
        var cakeDescription = $('#cakedescription-'+cakeId).text();
        var cakeImgUrl = cakeId == 0 ? "" : $('#cakeimg-'+cakeId).attr('src');
        var modalContents="";
        var modalTitle = cakeId == 0 ? "Add new cake" : "Edit cake";
        modalContents += "<form id=\"cakeForm-"+cakeId+"\" class=\"form-horizontal\">";
        modalContents += "<fieldset>";

        var nameInput="";
        nameInput += "<div class=\"form-group\" id=\"nameInput-"+cakeId+"\">";
        nameInput += "<label for=\"inputName\" class=\"col-lg-2 control-label\">Name<\/label>";
        nameInput += "<div class=\"col-lg-10\">";
        nameInput += "<input type=\"text\" class=\"form-control\" id=\"inputName-"+cakeId+"\" name=\"name\" placeholder=\"(required)\" value=\""+cakeName+"\">";
        nameInput += "<\/div>";
        nameInput += "<\/div>";

        var imgUrlInput="";
        imgUrlInput += "<div class=\"form-group\" id=\"urlInput-"+cakeId+"\">";
        imgUrlInput += "<label for=\"inputImage\" class=\"col-lg-2 control-label\">Image URL<\/label>";
        imgUrlInput += "<div class=\"col-lg-10\">";
        imgUrlInput += "<input type=\"text\" class=\"form-control\" id=\"inputImage-"+cakeId+"\" name=\"imageUrl\" placeholder=\"(optional, must start with http and end with .png or .jpg)\" value=\""+cakeImgUrl+"\">";
        imgUrlInput += "<\/div>";
        imgUrlInput += "<\/div>";

        var descriptionInput="";
        descriptionInput += "<div class=\"form-group\" id=\"descriptionInput-"+cakeId+"\">";
        descriptionInput += "<label for=\"description\" class=\"col-lg-2 control-label\">Description<\/label>";
        descriptionInput += "<div class=\"col-lg-10\">";
        descriptionInput += "<textarea class=\"form-control\" rows=\"3\" id=\"description-"+cakeId+"\" name=\"description\" placeholder=\"(required)\">"+cakeDescription+"<\/textarea>";
        descriptionInput += "<\/div>";
        descriptionInput += "<\/div>";

        modalContents += nameInput;
        modalContents += imgUrlInput;
        modalContents += descriptionInput;

        modalContents += "<\/fieldset>";
        modalContents += "<\/form>";
        bootbox.confirm({
            title: modalTitle,
            message: modalContents,
            callback: function(result){
                if(result){
                    $('.form-group').removeClass('has-error');
                    $('#form-failure').remove();

                    var nameToSubmit = $("#inputName-"+cakeId).val();

                    var nameErrors = nameValidationFailures(nameToSubmit,cakeId);

                    var descriptionToSubmit = $("#description-"+cakeId).val();
                    var descriptionErrors = descriptionValidationFailures(descriptionToSubmit,cakeId);

                    var imageUrlToSubmit = $("#inputImage-"+cakeId).val();
                    var urlErrors = urlValidationFailures(imageUrlToSubmit,cakeId);

                    if (nameErrors || descriptionErrors || urlErrors){
                        $('#cakeForm-'+cakeId).prepend(makeFailurePanel("Form validation failed",nameErrors + descriptionErrors + urlErrors));
                        return false;
                    }
                    return createOrUpdate(cakeId,nameToSubmit,descriptionToSubmit,imageUrlToSubmit);
                }
                else{
                    $('.form-group').removeClass('has-error');
                    $('#form-failure').remove();
                }
            }
        });
    })
});

function nameValidationFailures(name, cakeId){
    var nameFormGroup = $("#nameInput-"+cakeId);
    var failures = "";
    if (!name){
        failures += "<li>Name is a required field<\/li>";
        nameFormGroup.addClass('has-error');
    }
    else if (name.length < 1 || name.length > 100){
        failures += "<li>Name must have a length of 1-100 characters (you typed "+name.length+")</li>"
        nameFormGroup.addClass('has-error');
    }
    return failures;
}

function descriptionValidationFailures(description, cakeId){
    var descriptionFormGroup = $("#descriptionInput-"+cakeId);
    var failures = "";
    if (!description){
        failures += "<li>Description is a required field</li>";
        descriptionFormGroup.addClass('has-error');
    }
    else if (description.length < 1 || description.length > 300){
        failures += "<li>Description must have a length of between 1-300 characters (you typed "+description.length+")</li>"
        descriptionFormGroup.addClass('has-error');
    }
    return failures;
}

function urlValidationFailures(imageUrl, cakeId){
    var urlFormGroup = $("#urlInput-"+cakeId);
    var failures = "";
    if (imageUrl && (!imageUrl.startsWith("http") || !(imageUrl.endsWith(".jpg") || imageUrl.endsWith(".png")))){
        failures += "<li>URL, if present must start with http and end with .png or .jpg<\/li>";
        urlFormGroup.addClass('has-error');
    }
    if (imageUrl && (imageUrl.length > 300)){
        failures += "<li>Maximum image URL length is 300 characters<\/li>";
        urlFormGroup.addClass('has-error');
    }
    return failures;
}

function createOrUpdate(cakeId, submitTitle, submitDescription, submitImageUrl){
    // New cake
    if (cakeId ==0){
        $.ajax({
            url: '/cakes',
            type: 'POST',
            contentType: 'application/json',
            data:JSON.stringify({
                name: submitTitle,
                description: submitDescription,
                imageUrl: submitImageUrl
            }),
            success: function(){
                location.reload();
            },
            error: handleError
        });
    }
    // Updated cake
    else{
        $.ajax({
            url: '/cakes',
            type: 'PUT',
            contentType: 'application/json',
            data:JSON.stringify({
                id: cakeId,
                title: submitTitle,
                description: submitDescription,
                image: submitImageUrl
            }),
            success: function(){
                location.reload();
            },
            error: handleError
        });
    }
}

function handleError(err){
    var errors="<ul>";
    try {
        var responseJson = $.parseJSON(err.responseText);
        for (const jsonError of responseJson.errors){
            errors += "<li>"+jsonError+"<\/li>";
        }
    }
    catch(exception){
        errors+="<li>Unexpected error received from server<\/li>"
    }
    errors+="<\/ul>";
    bootbox.alert({
        title: "Sorry, the server rejected your data",
        message: errors
    });
}

function makeFailurePanel(panelTitle,failures){
    var failurePanel = "";
    failurePanel += "<div class=\"panel panel-danger\" id=\"form-failure\"'>";
    failurePanel += "<div class=\"panel-heading\">";
    failurePanel += "<h3 class=\"panel-title\">"+panelTitle+"<\/h3>";
    failurePanel += "<\/div>";
    failurePanel += "<div class=\"panel-body\">";
    failurePanel += "<ul>";
    failurePanel += failures;
    failurePanel += "   <\/ul>";
    failurePanel += "  <\/div>";
    failurePanel += "<\/div>";
    return failurePanel;
}