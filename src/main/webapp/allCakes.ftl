<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cake Manager</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-10">
            <h1>Cake Manager</h1>
        </div>
        <div class="col-sm-2">
            <span class="btn btn-success" data-cakeid="0">Add New Cake</span>
        </div>
    </div>
    <div id="cakes">
    <#if cakes?? && cakes?size != 0>
        <#list 0..cakes?size -1 as i>
            <#assign cake = cakes[i]>
            <#if i % 2 ==0>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 id="cakename-${cake.id}">${cake.name}</h2>
                        </div>
                    </div>
                    <div class="row">
                        <#if cake.imageUrl??>
                            <div class="col-md-6">
                                <img id="cakeimg-${cake.id}" class="img-responsive" src="${cake.imageUrl}"
                                     alt="${cake.name}"/>
                            </div>
                        </#if>
                        <div class="col-md-6">
                            <span id="cakedescription-${cake.id}">${cake.description}</span><br/>
                            <span class="btn btn-danger btn-xs" data-cakeid="${cake.id}">delete</span>
                            <span class="btn btn-primary btn-xs" data-cakeid="${cake.id}">edit</span>
                        </div>
                    </div>
                </div>
            <#else>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 id="cakename-${cake.id}">${cake.name}</h2>
                        </div>
                    </div>
                    <div class="row">
                        <#if cake.imageUrl??>
                            <div class="col-md-6">
                                <img id="cakeimg-${cake.id}" class="img-responsive" src="${cake.imageUrl}"
                                     alt="${cake.name}"/>
                            </div>
                        </#if>
                        <div class="col-md-6">
                            <span id="cakedescription-${cake.id}">${cake.description}</span><br/>
                            <span class="btn btn-danger btn-xs" data-cakeid="${cake.id}">delete</span>
                            <span class="btn btn-primary btn-xs" data-cakeid="${cake.id}">edit</span>
                        </div>
                    </div>
                </div>
            </#if>
            <#if i %2 ==1 || i == cakes?size - 1>
            </div>
            </#if>
        </#list>
    <#else>
        <div class="alert alert-danger">
            <strong>Oh no!</strong> No cakes received from the server. The cake is a lie?
        </div>
    </#if>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/cakes.js"></script>
</div>
</body>
</html>