package com.waracle.cakemgr.referencedata;

/**
 * Exception thrown if an error occurs while initialising reference data
 */
public class ReferenceDataException extends Exception {
  public ReferenceDataException(final Throwable cause) {
    super(cause);
  }
}