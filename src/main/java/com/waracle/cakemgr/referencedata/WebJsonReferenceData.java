package com.waracle.cakemgr.referencedata;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.dto.NewCakeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Generates reference data using web JSON file from original code sample
 */
@Component
public class WebJsonReferenceData implements CakeReferenceDataProvider {

  private static final Logger LOG = LoggerFactory.getLogger(WebJsonReferenceData.class);

  @Override
  public Set<NewCakeDto> getReferenceData() throws ReferenceDataException {
    try {
      ObjectMapper mapper = new ObjectMapper();
      URL json = new URL("https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json");
      JsonNode remoteJson = mapper.readTree(json);

      final Set<NewCakeDto> retrievedData = new HashSet<>();
      for (JsonNode jsonEntry : remoteJson) {
        retrievedData.add(new NewCakeDto(jsonEntry.get("title").textValue(), jsonEntry.get("desc").textValue(), jsonEntry.get("image").textValue()));
      }

      LOG.debug("Retrieve data set from remote JSON URL: {}", retrievedData);
      return Collections.unmodifiableSet(retrievedData);

    } catch (Exception e) {
      LOG.warn("Exception when attempting to retrieve reference data", e);
      throw new ReferenceDataException(e);
    }
  }
}
