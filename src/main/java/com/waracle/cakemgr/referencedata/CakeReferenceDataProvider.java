package com.waracle.cakemgr.referencedata;

import com.waracle.cakemgr.dto.NewCakeDto;

import java.util.Set;

/**
 * Helper service to obtain cake reference data for demo purposes
 */
public interface CakeReferenceDataProvider {

  /**
   * Obtains reference data for demonstrating the application
   *
   * @return reference data
   * @throws ReferenceDataException if retrieval of reference data fails
   */
  Set<NewCakeDto> getReferenceData() throws ReferenceDataException;
}
