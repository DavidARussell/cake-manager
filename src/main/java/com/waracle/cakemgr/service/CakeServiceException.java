package com.waracle.cakemgr.service;

/**
 * Exception thrown when certain CakeService operations fail
 */
public class CakeServiceException extends Exception {

  private CakeServiceException(String message) {
    super(message);
  }

  public static CakeServiceException cakeDoesNotExist(Integer cakeId) {
    return new CakeServiceException(String.format("The cake with ID %d was not found in the database", cakeId));
  }
}
