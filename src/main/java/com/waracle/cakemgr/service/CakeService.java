package com.waracle.cakemgr.service;

import com.waracle.cakemgr.dto.ExistingCakeDto;
import com.waracle.cakemgr.dto.NewCakeDto;

import java.util.List;

public interface CakeService {

  /**
   * Gets all cakes in the data store
   *
   * @return list of cake DTOs, not null
   */
  List<ExistingCakeDto> getAllCakes();

  /**
   * Saves a new cake to the data store
   *
   * @param newCake cake data to save, not null
   * @return saved data (including ID)
   */
  ExistingCakeDto saveNewCake(NewCakeDto newCake);

  /**
   * Deletes a cake
   *
   * @param cakeId the cake to delete
   * @throws CakeServiceException if unable to delete
   */
  void deleteCake(Integer cakeId) throws CakeServiceException;

  /**
   * Updates a cake in the database
   *
   * @param updatedCake updated cake data
   * @return persisted data
   * @throws CakeServiceException if unable to update
   */
  ExistingCakeDto updateCake(ExistingCakeDto updatedCake);
}
