package com.waracle.cakemgr.service;

import com.waracle.cakemgr.dto.ExistingCakeDto;
import com.waracle.cakemgr.dto.NewCakeDto;
import com.waracle.cakemgr.model.CakeEntity;
import com.waracle.cakemgr.model.CakeRepository;
import com.waracle.cakemgr.referencedata.CakeReferenceDataProvider;
import com.waracle.cakemgr.referencedata.ReferenceDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CakeServiceImpl implements CakeService {

  private static final Logger LOG = LoggerFactory.getLogger(CakeServiceImpl.class);
  private static final String STRING_LENGTH_ERROR = "%s must have a length of between 1 and %d characters";

  private final CakeRepository cakeRepository;
  private final CakeReferenceDataProvider cakeReferenceDataProvider;

  public CakeServiceImpl(CakeRepository cakeRepository) {
    this(cakeRepository, null);
  }

  @Autowired
  public CakeServiceImpl(CakeRepository cakeRepository, CakeReferenceDataProvider cakeReferenceDataProvider) {
    this.cakeRepository = cakeRepository;
    this.cakeReferenceDataProvider = cakeReferenceDataProvider;
    if (cakeReferenceDataProvider != null) {
      try {
        for (NewCakeDto referenceCake : cakeReferenceDataProvider.getReferenceData()) {
          this.saveNewCake(referenceCake);
        }
      } catch (ReferenceDataException rde) {
        LOG.error("Error when attempting to retrieve reference data from web, using backup data instead", rde);
        loadBackupHardcodedData();
      }
    } else {
      loadBackupHardcodedData();
    }
  }

  /**
   * Helper method to convert a database entity to an output DTO
   *
   * @param entity the entity to convert
   * @return an output DTO containing that entity's data
   */
  private static ExistingCakeDto makeOutputDtoFromEntity(CakeEntity entity) {
    if (entity == null) {
      return null;
    }
    return ExistingCakeDto.builder().id(entity.getId()).title(entity.getName()).description(entity.getDescription()).image(entity.getImageUrl()).errors(Collections.emptyList()).build();
  }

  /**
   * Helper method to convert an input DTO to a database entity
   *
   * @param inputDto the input data to convert
   * @return a detached database entity containing the input DTO's data
   */
  private static CakeEntity makeEntityFromInputDto(NewCakeDto inputDto) {
    if (inputDto == null) {
      return null;
    }
    return CakeEntity.builder().name(inputDto.getName()).description(inputDto.getDescription()).imageUrl(inputDto.getImageUrl()).build();
  }

  /**
   * Validates input data
   *
   * @param newCake input data to validate
   * @return validation errors, not null (but may be empty)
   */
  private static List<String> validate(NewCakeDto newCake) {
    List<String> validationErrors = new ArrayList<>();
    validationErrors.addAll(validateName(newCake));
    validationErrors.addAll(validateImageUrl(newCake));
    validationErrors.addAll(validateDescription(newCake));
    return validationErrors.isEmpty() ? Collections.emptyList() : Collections.unmodifiableList(validationErrors);
  }

  /**
   * Validates name field of input data
   *
   * @param newCake input data to validate
   * @return validation errors, not null (but may be empty)
   */
  private static List<String> validateName(NewCakeDto newCake) {
    String name = newCake.getName();
    if (name == null) {
      return Collections.singletonList("name is a required field");
    } else if (name.trim().length() < 1 || name.length() > CakeEntity.MAX_TITLE_LENGTH) {
      return Collections.singletonList(String.format("%s must have a length of between 1 and %d characters", "name", CakeEntity.MAX_TITLE_LENGTH));
    } else {
      return Collections.emptyList();
    }
  }

  /**
   * Validates imageUrl field of input data
   *
   * @param newCake input data to validate
   * @return validation errors, not null (but may be empty)
   */
  private static List<String> validateImageUrl(NewCakeDto newCake) {
    String imageUrl = newCake.getImageUrl();
    List<String> validationErrors = new ArrayList<>();
    if (imageUrl != null) {
      if (!imageUrl.startsWith("http")) {
        validationErrors.add("imageUrl must start with http");
      }
      if (!imageUrl.endsWith(".png") && !imageUrl.endsWith(".jpg")) {
        validationErrors.add("imageUrl must end with .jpg or .png");
      }
    }
    return validationErrors.isEmpty() ? Collections.emptyList() : Collections.unmodifiableList(validationErrors);
  }

  /**
   * Validates description field of input data
   *
   * @param newCake input data to validate
   * @return validation errors, not null (but may be empty)
   */
  private static List<String> validateDescription(NewCakeDto newCake) {
    String description = newCake.getDescription();
    List<String> validationErrors = new ArrayList<>();
    if (description == null) {
      validationErrors.add("description is a required field");
    } else if (description.trim().length() < 1 || description.length() > CakeEntity.MAX_DESCRIPTION_LENGTH) {
      validationErrors.add(String.format(STRING_LENGTH_ERROR, "description", CakeEntity.MAX_DESCRIPTION_LENGTH));
    }
    return validationErrors.isEmpty() ? Collections.emptyList() : Collections.unmodifiableList(validationErrors);
  }

  @Override
  public List<ExistingCakeDto> getAllCakes() {
    return cakeRepository.findAll().stream().map(CakeServiceImpl::makeOutputDtoFromEntity).collect(Collectors.toList());
  }

  @Override
  public ExistingCakeDto saveNewCake(NewCakeDto newCake) {
    LOG.debug("Saving new cake: {}", newCake);
    final List<String> validationErrors = validate(newCake);
    if (validationErrors.isEmpty()) {
      return makeOutputDtoFromEntity(cakeRepository.save(makeEntityFromInputDto(newCake)));
    }
    LOG.info("Input data failed validation: {}", validationErrors);
    return ExistingCakeDto.builder().errors(validationErrors).build();
  }

  @Override
  public void deleteCake(Integer cakeId) throws CakeServiceException {
    CakeEntity cakeToDelete = getRequiredSingleCake(cakeId);
    cakeRepository.delete(cakeToDelete);
  }

  @Override
  public ExistingCakeDto updateCake(ExistingCakeDto updatedCake) {
    try {
      CakeEntity cakeToUpdate = getRequiredSingleCake(updatedCake.getId());
      final List<String> validationErrors = validate(updatedCake);
      if (validationErrors.isEmpty()) {
        cakeToUpdate.setName(updatedCake.getName());
        cakeToUpdate.setDescription(updatedCake.getDescription());
        cakeToUpdate.setImageUrl(updatedCake.getImageUrl());
        return makeOutputDtoFromEntity(cakeRepository.save(cakeToUpdate));
      }
      return ExistingCakeDto.builder().errors(validationErrors).build();
    } catch (CakeServiceException cse) {
      LOG.warn("Failed to save update due to exception", cse);
      return ExistingCakeDto.builder().errors(Collections.singletonList(String.format("Unable to update cake ID %d", updatedCake.getId()))).build();
    }
  }

  /**
   * Helper method to retrieve a cake
   *
   * @param cakeId ID of cake to find
   * @return cake entity from data layer
   * @throws CakeServiceException if no cake is found
   */
  private CakeEntity getRequiredSingleCake(Integer cakeId) throws CakeServiceException {
    CakeEntity cakeToDelete = cakeRepository.findOne(cakeId);
    if (cakeToDelete == null) {
      throw CakeServiceException.cakeDoesNotExist(cakeId);
    }
    return cakeToDelete;
  }

  /**
   * If retrieving the reference data fails for some reason (e.g. the application is being tested offline) this method
   * puts some hardcoded backup data into the DB instead
   */
  private void loadBackupHardcodedData() {
    CakeEntity trialCake = CakeEntity.builder()
        .name("Victoria Sponge")
        .description("Two layers of sponge cake with jam and buttercream between them, dusted with icing sugar.")
        .imageUrl("http://www.bbcgoodfood.com/sites/bbcgoodfood.com/files/recipe_images/recipe-imageUrl-legacy-id--1001468_10.jpg")
        .build();
    cakeRepository.save(trialCake);
  }

}
