package com.waracle.cakemgr.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@DynamicUpdate
@Table(name = "Cake")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CakeEntity implements Serializable {

  public static final int MAX_TITLE_LENGTH = 100;
  public static final int MAX_DESCRIPTION_LENGTH = 300;
  public static final int MAX_IMAGE_URL_LENGTH = 300;
  private static final long serialVersionUID = -1798070786993154675L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "NAME", nullable = false, length = MAX_TITLE_LENGTH)
  private String name;

  @Column(name = "DESCRIPTION", nullable = false, length = MAX_DESCRIPTION_LENGTH)
  private String description;

  @Column(name = "IMAGE_URL", length = MAX_IMAGE_URL_LENGTH)
  private String imageUrl;

}