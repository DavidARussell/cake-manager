package com.waracle.cakemgr.controller;

import com.waracle.cakemgr.dto.CakeDeletionDto;
import com.waracle.cakemgr.dto.ExistingCakeDto;
import com.waracle.cakemgr.dto.NewCakeDto;
import com.waracle.cakemgr.service.CakeService;
import com.waracle.cakemgr.service.CakeServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class CakeController {

  private static final Logger LOG = LoggerFactory.getLogger(CakeController.class);

  private static final String ENDPOINT = "/cakes";

  private final CakeService cakeService;

  @Autowired
  public CakeController(CakeService cakeService) {
    this.cakeService = cakeService;
  }

  /**
   * Web rendering endpoint for GET /cakes
   *
   * @param model context object for the template renderer
   * @return template to be rendered
   */
  @RequestMapping(value = ENDPOINT, method = RequestMethod.GET)
  public String getAllCakesWebpage(Model model) {
    LOG.debug("Rendering all cakes web view");
    model.addAttribute("cakes", cakeService.getAllCakes());
    return "allCakes";
  }

  /**
   * JSON endpoint for getting the list of cakes
   *
   * @return list of cakes
   */
  @RequestMapping(value = ENDPOINT, method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
  @ResponseBody
  public List<ExistingCakeDto> getAllCakesJson() {
    LOG.debug("Retrieving all cakes JSON");
    return cakeService.getAllCakes();
  }

  /**
   * JSON endpoint for adding a new cake
   *
   * @param newCake new cake to add
   * @return generated cake object
   */
  @RequestMapping(value = ENDPOINT, method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
  @ResponseBody
  public ResponseEntity<ExistingCakeDto> saveNewCakeJson(@RequestBody NewCakeDto newCake) {
    LOG.debug("Saving new cake JSON input");
    return saveNewCake(newCake);
  }

  /**
   * JSON endpoint for deleting a cake
   *
   * @param cakeToDelete DTO containing details of the cake to delete
   * @return empty response with HTTP status code indicating outcome
   */
  @RequestMapping(value = ENDPOINT, method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
  @ResponseBody
  public ResponseEntity deleteCake(@Validated @RequestBody CakeDeletionDto cakeToDelete) {
    LOG.debug("Received deletion request for cake id {}", cakeToDelete.getId());
    try {
      cakeService.deleteCake(cakeToDelete.getId());
      LOG.debug("Successfully deleted cake id {}", cakeToDelete.getId());
      return ResponseEntity.ok().build();
    } catch (CakeServiceException cse) {
      LOG.warn("Failed to delete cake ID {} due to exception", cakeToDelete.getId(), cse);
      return ResponseEntity.badRequest().build();
    }
  }

  /**
   * JSON endpoint for updating an existing cake
   *
   * @param updatedCake details of the cake to update
   * @return DTO containing persisted cake update or error messages
   */
  @RequestMapping(value = ENDPOINT, method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
  @ResponseBody
  public ResponseEntity<ExistingCakeDto> updateCake(@RequestBody @Validated ExistingCakeDto updatedCake) {
    LOG.debug("Received updated cake data: {}", updatedCake);
    ExistingCakeDto outputDto = cakeService.updateCake(updatedCake);
    if (outputDto.getErrors().isEmpty()) {
      return ResponseEntity.ok().body(outputDto);
    }
    return ResponseEntity.badRequest().body(outputDto);
  }

  /**
   * Helper method to provide common error-handling functionality for both 'new cake' endpoints
   *
   * @param newCake the input DTO to save
   * @return server response to input
   */
  private ResponseEntity<ExistingCakeDto> saveNewCake(NewCakeDto newCake) {
    final ExistingCakeDto response = cakeService.saveNewCake(newCake);
    if (response.getErrors().isEmpty()) {
      return ResponseEntity.ok(response);
    }
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }
}
