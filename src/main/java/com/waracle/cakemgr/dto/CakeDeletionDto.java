package com.waracle.cakemgr.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class CakeDeletionDto {

  @NotNull
  @Min(1)
  private final Integer id;
}
