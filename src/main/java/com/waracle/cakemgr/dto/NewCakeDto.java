package com.waracle.cakemgr.dto;

import lombok.Data;

/**
 * DTO used for inputting a new cake to the application
 */
@Data
public class NewCakeDto {

  private final String name;
  private final String description;
  private final String imageUrl;

}
