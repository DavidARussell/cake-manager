package com.waracle.cakemgr.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Collections;
import java.util.List;

/**
 * DTO used for outputting a cake from the system
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ExistingCakeDto extends NewCakeDto {

  private final Integer id;
  private final List<String> errors;

  public ExistingCakeDto(@JsonProperty("title") String title, @JsonProperty("description") String description, @JsonProperty("image") String image, @JsonProperty("id") Integer id) {
    super(title, description, image);
    this.id = id;
    this.errors = Collections.emptyList();
  }

  @Builder
  public ExistingCakeDto(String title, String description, String image, Integer id, List<String> errors) {
    super(title, description, image);
    this.id = id;
    this.errors = errors != null ? Collections.unmodifiableList(errors) : Collections.emptyList();
  }
}
