package com.waracle.cakemgr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class CakeManager extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(CakeManager.class, args);
  }
}
